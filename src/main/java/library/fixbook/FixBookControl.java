package library.fixbook;
import library.entities.Book;
import library.entities.Library;

public class FixBookControl {
	
	private FixBookUI UI;
	private enum ControlState { INITIALISED, READY, FIXING };
	private ControlState state;
	
	private Library library;
	private Book currentBook;


	public FixBookControl() {
		this.library = Library.getInstance();
		state = ControlState.INITIALISED;
	}
	
	
	public void setUI(FixBookUI UI) {
		if (!state.equals(ControlState.INITIALISED))
			throw new RuntimeException("FixBookControl: cannot call setUI except in INITIALISED state");
			
		this.UI = UI;
		UI.setState(FixBookUI.UIState.READY);
		state = ControlState.READY;
	}


	public void bookScanned(int bookId) {
		if (!state.equals(ControlState.READY))
			throw new RuntimeException("FixBookControl: cannot call bookScanned except in READY state");
			
		currentBook = library.getBook(bookId);
		
		if (currentBook == null) {
			UI.display("Invalid bookId");
			return;
		}
		if (!currentBook.isDamaged()) {
			UI.display("Book has not been damaged");
			return;
		}
		UI.display(currentBook.toString());
		UI.setState(FixBookUI.UIState.FIXING);
		state = ControlState.FIXING;
	}


	public void fixBook(boolean mustFix) {
		if (!state.equals(ControlState.FIXING))
			throw new RuntimeException("FixBookControl: cannot call fixBook except in FIXING state");
			
		if (mustFix)
			library.repairBook(currentBook);
		
		currentBook = null;
		UI.setState(FixBookUI.UIState.READY);
		state = ControlState.READY;
	}

	
	public void scanningComplete() {
		if (!state.equals(ControlState.READY))
			throw new RuntimeException("FixBookControl: cannot call scanningComplete except in READY state");
			
		UI.setState(FixBookUI.UIState.COMPLETED);
	}

}
